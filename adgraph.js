let myGraph = {
    "nodes": [{
            "id": 1,
            "relationships": []
        },
        {
            "id": 2,
            "relationships": [{
                "nodeId": 1
            }]
        },
        {
            "id": 3,
            "relationships": [{
                    "nodeId": 2
                },
                {
                    "nodeId": 1
                },
            ]
        }
    ]
};


function readGraph() {
    let nodes = myGraph["nodes"];
    //Iterate over the array of nodes
    for (let i = 0; i < nodes.length; i++) {
        path = nodePath(nodes[i]);
        console.log("Esta es la ruta para el nodo " + nodes[i]["id"] + ":" + path);
    }
}

function nodePath(node) {

    let path = "";
    let nodeId = node["id"];

    let nodeRelationships = node["relationships"];

    if (nodeRelationships.length > 0) {
        for (let i = 0; i < nodeRelationships.length; i++) {
            let newNode = findNode(nodeRelationships[i]["nodeId"]);
            let deepPath = nodePath(newNode);
            path += "." + deepPath;
        }
    }

    return nodeId + "" + path;

}

function findNode(id) {

    let nodes = myGraph["nodes"];
    //Iterate over the array of nodes
    for (let i = 0; i < nodes.length; i++) {
        if (nodes[i]["id"] == id) {
            console.log("Encontré el nodo:" + id);
            return nodes[i];
        }
    }

    return false;

}